# MattermostHooks

## IndicoReminder

This can be used to query (for now it just checks if a certain substring is in Event-name) indico events in a certain category. 
It will then send out reminders to a Mattermost-Channel (using a web-hook the user must create and provide the hook-key for).

To set-up a scheduled pipeline in gitlab, just follow these steps:

- first define the necessary secrets as masked&protected variables for the CI under Settings->CI/CD->Variables:
  - click Expand & and add the following Variables (**make sure to tick the box for both "Protect variable" & "Mask variable" !!!**):
    - `INDICO_API_KEY`
    - `INDICO_SIGN`
    - `MATTERMOST_SERVER`
    - `MATTERMOST_HOOK_KEY`
- add schedule to CI/CD -> Schedules
  - specify the variables `EVENT_NAME_PATTERN` and `INDICO_CAT` here
- done. your mattemost channel should now recieve the messages on your specified schedule for each event within the hour.
