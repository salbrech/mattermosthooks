import requests, os, arrow
from ics import Calendar,Event


class IndicoReminder:
    def __init__(self, event_name_pattern, indico_cat,indico_api_key, indico_signature, mattermost_server, mattermost_hook_key, indico_server, debug=False, dt=60):
        self._event_name_pattern = event_name_pattern
        self._indico_cat = indico_cat

        self._indico_server = indico_server
        self.__api_key = indico_api_key
        self.__signature = indico_signature

        self._mattermost_server = mattermost_server
        self.__hook_key = mattermost_hook_key
        
        self._now = arrow.now()
        self._day_str = self._now.strftime("%d_%m_%Y")
        
        self._debug = debug

        self._dt = dt
        
        self._upcoming_events = None
        
    def parse_indico(self,data_dir='data'):
    
        api_request = f"/export/categ/{self._indico_cat}.ics?apikey={self.__api_key}&from=-31d&signature={self.__signature}"
    
        if(not os.path.isdir(data_dir)):
            os.makedirs(data_dir)
        
        ical_file = data_dir+f'/{self._indico_cat}_events_{self._day_str}.ics'
        if(not os.path.isfile(ical_file)):
            print('getting fresh calendar from indico... ')
            url = self._indico_server+api_request
            r = requests.get(url, allow_redirects=True)
            open(ical_file, 'wb').write(r.content)
        
        c = Calendar(open(ical_file,'r').read())

        if(self._debug):
            event_url = "https://indico.cern.ch/event/1022192/"
            e = Event()
            e.name = self._event_name_pattern+ " 1"
            e.begin = self._now.shift(hours=1,minutes = -1)
            e.end = e.begin.shift(hours=1)
            e.url = event_url
            c.events.add(e)
            e = Event()
            e.name = self._event_name_pattern+ " 2"
            e.begin = self._now.shift(minutes = 24)
            e.end = e.begin.shift(hours=1)
            e.url = event_url
            c.events.add(e)
            e = Event()
            e.name = self._event_name_pattern + " 3"
            e.begin = self._now.shift(hours=1,seconds = +20)
            e.end = e.begin.shift(hours=1)
            e.url = event_url
            c.events.add(e)
        filtered_events = [e for e in c.events if (self._event_name_pattern.lower() in e.name.lower())]
        self._upcoming_events = [e for e in filtered_events if (e.begin - self._now).total_seconds()>0]
        return self._upcoming_events

    def send_reminder(self,event, remove_event_if_reminded = False):
        self._now = arrow.now()
        event_begin_str = event.begin.strftime("%H:%M")
        
        message = f"Hello everyone! \n This is a friendly reminder that the next Meeting [{event.name}]({event.url}) starts ***{event.begin.humanize()}***! \n See you soon! :spiral_calendar:"
        
        dt = (event.begin - self._now).total_seconds()
        if(dt/60.0<self._dt):
            headers = {'Content-Type': 'application/json',}
            values = '{ "text": "%s"}'%message
            print('sending message: '+message)
            response = requests.post(f'http://{self._mattermost_server}/hooks/{self.__hook_key}', headers=headers, data=values)
            if(remove_event_if_reminded):
                self._upcoming_events.remove(event)
            
    def process_reminders(self):
        if(self._upcoming_events is None):
            self.parse_indico()
        for event in self._upcoming_events:
            self.send_reminder(event)

    def loop_process(self):
        import time
        if(self._upcoming_events is None):
            self.parse_indico()
        while True:
            self.process_reminders()
            if(len(self._upcoming_events)==0):
                break
            for event in self._upcoming_events:
                self.send_reminder(event, remove_event_if_reminded = True)
            time.sleep(10)
        print("no more upcoming events")

if(__name__ == '__main__'):
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--indico_cat', type=str, help='The indico-category that contains the events that will be subject of query', required=True)
    parser.add_argument('--event_name_pattern', type=str, help='Pattern that should be contained in the event name(s) that will be subject of query', required=True)

    parser.add_argument('-t','--dt', type=float, default=60.0, help='Specifies how many minutes, before the event starts, the reminder should be send')
    
    parser.add_argument('--indico_api', type=str, help='SECRET Indico-API key.', required=True)
    parser.add_argument('--indico_sign', type=str, help='SECRET Indico Signature.', required=True)
    parser.add_argument('--indico_server', type=str, help='The indico-server the query for upcoming events should be made on.', default = 'https://indico.cern.ch/')

    parser.add_argument('--mattermost_hook', type=str, help='SECRET Mattermost-Webhook key.', required=True)
    parser.add_argument('--mattermost_server', type=str, help='The Mattermost-server the reminder should be send to.', required=True)

    parser.add_argument('--loop', action = 'store_true', help='Query events from indico and send reminders in specified tiemframe before the event starts, until there are no remaining events from inital query left.')

    parser.add_argument('--once', action = 'store_true', help='Query events from indico and send reminders about all events that are currently starting within specified timeframe.')

    parser.add_argument('--debug', action = 'store_true', help = 'adding two events to query results. One in 59 minutes and one in 60 minutes and 20 seconds in order to test workflow')

    
    
    args = parser.parse_args()

    IR = IndicoReminder(event_name_pattern = args.event_name_pattern, indico_cat = args.indico_cat,
                    indico_api_key = args.indico_api,
                    indico_signature = args.indico_sign,
                    indico_server = args.indico_server,
                    mattermost_server = args.mattermost_server,
                    mattermost_hook_key = args.mattermost_hook,
                    debug = args.debug,
                    dt=args.dt)
    if(args.loop):
        IR.loop_process()
    if(args.once):
        IR.process_reminders()
